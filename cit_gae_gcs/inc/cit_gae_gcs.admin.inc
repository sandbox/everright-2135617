<?php

/**
 * @file
 * Admin page callbacks for the cit_gae_gcs module.
 */

/**
 * Creation of configuration settings form for callback.
 */
function cit_gae_gcs_admin_settings_form($form, &$form_state) {
  $form['cit_gae_gcs_bucket'] = array(
    '#type' => 'textfield',
    '#title' => 'Default Bucket',
    '#maxlength' => 63,
    '#required' => TRUE,
    '#default_value' => variable_get('cit_gae_gcs_bucket', 'drupal'),
    '#description' => t('You must follow bucket and object naming guidelines when you use the Google Cloud Storage API. Requests that use malformed or disallowed names will often fail. !gcs-bucket-doc.', array('!gcs-bucket-doc' => l('More details', 'https://developers.google.com/storage/docs/bucketnaming'))),
  );

  $form['cit_gae_gcs_domain'] = array(
    '#type' => 'textfield',
    '#title' => 'Domain',
    '#maxlength' => 128,
    '#required' => TRUE,
    '#default_value' => variable_get('cit_gae_gcs_domain', 'https://storage.googleapis.com/'),
    '#description' => t('You can access Google Cloud Storage through three request endpoints (URIs). Which one you use depends on the operation you are performing. !gcs-domain-doc.', array('!gcs-domain-doc' => l('More details', 'https://developers.google.com/storage/docs/reference-uris'))),
  );

  $form['cit_gae_gcs_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Debug'),
    '#default_value' => variable_get('cit_gae_gcs_debug', FALSE),
    '#description' => t('Check the box to enable logging.'),
  );

  $form['#validate'][] = 'cit_gae_gcs_admin_settings_form_validate';

  return system_settings_form($form);
}

/**
 * Configuration settings form validation.
 */
function cit_gae_gcs_admin_settings_form_validate($form, &$form_state) {
  // check bucket name is correct.
}
