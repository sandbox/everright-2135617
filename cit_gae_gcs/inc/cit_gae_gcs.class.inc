<?php

/**
 * A user space stream wrapper for reading and writing to Google Cloud Storage.
 * See: http://www.php.net/manual/en/class.streamwrapper.php
 */

/**
 * GCS stream wrapper base class for local files.
 *
 * This class provides a complete stream wrapper implementation. URIs such as
 * "public://example.txt" are expanded to a normal filesystem path such as
 * "sites/default/files/example.txt" and then PHP filesystem functions are
 * invoked.
 *
 * GCSLocalStreamWrapper implementations need to implement at least the
 * getDirectoryPath() and getExternalUrl() methods.
 */
class GCSLocalStreamWrapper extends DrupalLocalStreamWrapper {
  //const ALLOWED_BUCKET_INI = 'google_app_engine.allow_include_gs_buckets';
  /**
   * base url.
   *
   * @var String
   */
  protected $base_url = NULL;
  /**
   * bucket.
   *
   * @var String
   */
  protected $bucket = NULL;
  /**
   * Constructs a new stream wrapper.
   */
  public function __construct() {
    $base_url = variable_get('cit_gae_gcs_domain', 'https://storage.googleapis.com');
    $this->base_url = trim($base_url, '\/');
    $bucket = variable_get('cit_gae_gcs_bucket', 'drupal');
    $this->bucket = trim($bucket, '\/');
    /*
    $options = array('gs' => array('acl' => 'public-read'));
	$metadata = array('foo' => 'far', 'bar' => 'boo');
	$ctx = array(
	  'gs' => array(
		'acl' => 'public-read',
		'metadata' => $metadata,
	  ),
	);
    stream_context_set_default($ctx);
    */
  }

  /**
   * Gets the path that the wrapper is responsible for.
   * @TODO: Review this method name in D8 per http://drupal.org/node/701358
   *
   * @return
   *   String specifying the path.
   */
  function getDirectoryPath() {
    return $this->bucket;
  }

  /**
   * Returns the canonical absolute path of the URI, if possible.
   *
   * @param string $uri
   *   (optional) The stream wrapper URI to be converted to a canonical
   *   absolute path. This may point to a directory or another type of file.
   *
   * @return string|false
   *   If $uri is not set, returns the canonical absolute path of the URI
   *   previously set by the DrupalStreamWrapperInterface::setUri() function.
   *   If $uri is set and valid for this class, returns its canonical absolute
   *   path, as determined by the realpath() function. If $uri is set but not
   *   valid, returns FALSE.
   */
  protected function getLocalPath($uri = NULL) {
    if (!isset($uri)) {
      $uri = $this->uri;
    }
    $path = 'gs://' . $this->getDirectoryPath() . '/' . $this->getTarget($uri);

    return $path;
  }

  /**
   * Overrides getExternalUrl().
   *
   * Return the HTML URI of a public file.
   */
  function getExternalUrl() {
    $path = str_replace('\\', '/', $this->getTarget());
    return $this->base_url . '/' . self::getDirectoryPath() . '/' . drupal_encode_path($path);
  }
}

/**
 * GCS public (public://) stream wrapper class.
 *
 * Provides support for storing publicly accessible files with the Drupal file
 * interface.
 */
class GCSPublicStreamWrapper extends GCSLocalStreamWrapper {
  /**
   * Constructs a new stream wrapper.
   */

  function __construct() {
    parent::__construct();
    $options = array('gs' => array('acl' => 'public-read'));
    //$this->context = stream_context_create($options);
    stream_context_set_default($options);
  }

  /**
   * Implements abstract public function getDirectoryPath()
   */
  public function getDirectoryPath() {
    return $this->bucket . '/' . variable_get('file_public_path', '');
  }

  /**
   * Overrides getExternalUrl().
   *
   * Return the HTML URI of a public file.
   */
  function getExternalUrl() {
    $path = str_replace('\\', '/', $this->getTarget());
    return $this->base_url . '/' . self::getDirectoryPath() . '/' . drupal_encode_path($path);
  }
}

/**
 * GCS private (private://) stream wrapper class.
 *
 * Provides support for storing privately accessible files with the Drupal file
 * interface.
 *
 * Extends GCSLocalStreamWrapper.
 */
class GCSPrivateStreamWrapper extends GCSLocalStreamWrapper {
  /**
   * Implements abstract public function getDirectoryPath()
   */
  public function getDirectoryPath() {
    return $this->bucket . '/' . variable_get('file_private_path', '');
  }

  /**
   * Overrides getExternalUrl().
   *
   * Return the HTML URI of a private file.
   */
  function getExternalUrl() {
    $path = str_replace('\\', '/', $this->getTarget());
    return url('system/files/' . $path, array('absolute' => TRUE));
  }
}

/**
 * GCS temporary (temporary://) stream wrapper class.
 *
 * Provides support for storing temporarily accessible files with the Drupal
 * file interface.
 *
 * Extends DrupalLocalStreamWrapper.
 */
class GCSTemporaryStreamWrapper extends GCSLocalStreamWrapper {
  /**
   * Implements abstract public function getDirectoryPath()
   */
  public function getDirectoryPath() {
    return $this->bucket . '/' . variable_get('file_temporary_path', '');
  }

  /**
   * Overrides getExternalUrl().
   */
  public function getExternalUrl() {
    $path = str_replace('\\', '/', $this->getTarget());
    return url('system/temporary/' . $path, array('absolute' => TRUE));
  }
}

