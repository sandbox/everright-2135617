<?php

/**
 * @file
 * Cit_Gae_User primary module file.
 */

define('CIT_GAE_USER_CALLBACK', 'cit_gae_user_callback');
define('CIT_GAE_USER_LOGIN', 'cit_gae_user_login');

// Google users service sdk.
require_once 'google/appengine/api/users/UserService.php';
use google\appengine\api\users\User;
use google\appengine\api\users\UserService;

/**
 * Implements hook_menu().
 */
function cit_gae_user_menu() {
  $items = array();
  $items['cit_gae_user/authenticate'] = array(
    'title' => 'Google Login',
    'page callback' => 'cit_gae_user_authentication',
    'access callback' => 'user_is_anonymous',
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * cit_gae_user_authentication_page.
 */
function cit_gae_user_authentication() {
  $callback = NULL;
  if (isset($_SESSION[CIT_GAE_USER_CALLBACK])) {
    $callback = $_SESSION[CIT_GAE_USER_CALLBACK];
    unset($_SESSION[CIT_GAE_USER_CALLBACK]);
  }
  $login = NULL;
  if (isset($_SESSION[CIT_GAE_USER_LOGIN])) {
    $login = $_SESSION[CIT_GAE_USER_LOGIN];
    unset($_SESSION[CIT_GAE_USER_LOGIN]);
  }

  if ('google' !== $login) {
    return;
  }

  $google = UserService::getCurrentUser();
  if ($google) {
    $exist = TRUE;
    $uid = cit_gae_user_google_load($google->getUserId());
    if (empty($uid)) {
      $uid = cit_gae_user_get_uid($google->getEmail());
      if (empty($uid)) {
        $exist = FALSE;
        $user = new stdClass();
        $user->mail = $google->getEmail();
        $user->name = cit_gae_user_init_name($google->getEmail());
        $user->is_new = TRUE;
        $user->status = 1;
        $new_user = user_save($user);
        $uid = $new_user->uid;
      }

      $gauth = array(
        'google_id' => $google->getUserId(),
        'uid' => $uid,
      );
      drupal_write_record('cit_gae_user', $gauth);
    }
    $form_state['uid'] = $uid;
    user_login_submit(array(), $form_state);

    if (FALSE === $exist) {
      global $user;
      $token = drupal_hash_base64(drupal_random_bytes(55));
      $_SESSION['pass_reset_' . $user->uid] = $token;
      drupal_set_message(t("Click <a href=@url target=_blank>here</a> to set password", array('@url' => url('user/' . $user->uid . '/edit', array('query' => array('pass-reset-token' => $token))))), 'warning');
    }
  }

  drupal_goto($callback);
  exit;
}

/**
 * Implements hook_form_alter().
 */
function cit_gae_user_form_alter(&$form, &$form_state, $form_id) {
  if ('user_login' ==  $form_id || 'user_login_block' == $form_id) {
    $form['google_signin'] = array(
      '#type' => 'submit',
      '#value' => '',
      '#submit' => array('cit_gae_user_login_submit'),
      '#limit_validation_errors' => array(),
      '#weight' => 1000,
    );

    $css = drupal_get_path('module', 'cit_gae_user') . '/css/cit_gae_user.css';
    if (isset($form['#attached']['css']) && !empty($form['#attached']['css'])) {
      $form['#attached']['css'][] = $css;
    }
    else {
      $form['#attached']['css'] = array($css);
    }
  }
}

/**
 * cit_gae_user_login_submit
 */
function cit_gae_user_login_submit() {
  $_SESSION[CIT_GAE_USER_CALLBACK] = ltrim(request_uri(), '/');
  $_SESSION[CIT_GAE_USER_LOGIN] = 'google';
  $login_url = UserService::createLoginURL(url('cit_gae_user/authenticate'));
  drupal_goto($login_url);
  exit;
}

/**
 * Create a username from an email address.
 *
 * @param string $mail
 *   Email address.
 *
 * @return string
 *   User name.
 */
function cit_gae_user_init_name($mail) {
  preg_match("/^(.+)@.+\..+/", $mail, $matches);
  $temp_name = $matches[1];
  $name = $temp_name;
  $count = 0;
  $exist = 1;
  while ($exist) {
    if ($count > 0) {
      $name = $temp_name . $count;
    }
    $exist = cit_gae_user_get_uid($name, 'name');
    $count++;
  }
  return $name;
}

/**
 * Get user id by name or mail.
 *
 * @param string $value
 *   Values should be [name|mail].
 *
 * @return int
 *   User id.
 */
function cit_gae_user_get_uid($value, $field = 'mail') {
  $uid = db_select('users', 'u')
    ->fields('u', array('uid'))
    ->condition($field, $value)
    ->execute()
    ->fetchField();
  return $uid;
}

/**
 * Function returns uid of passed google id
 */
function cit_gae_user_google_load($value, $field = 'google_id') {
  $uid = db_select('cit_gae_user', 'u')
    ->fields('u', array('uid'))
    ->condition($field, $value)
    ->execute()
    ->fetchField();
  return $uid;
}

/**
 * Implmenentation of hook_user_delete();
 */
function cit_gae_user_user_delete($user) {
  db_delete('cit_gae_user')
    ->condition('uid', $user->uid)
    ->execute();
}

