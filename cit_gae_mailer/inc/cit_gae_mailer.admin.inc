<?php

/**
 * @file
 * Admin page callbacks for the cit_gae_mailer module.
 */

/**
 * Creation of configuration settings form for callback.
 */
function cit_gae_mailer_admin_settings_form($form, &$form_state) {
  $form['cit_gae_mailer_fallback_email'] = array(
    '#type' => 'textfield',
    '#title' => 'Fallback email',
    '#maxlength' => 128,
    '#default_value' => variable_get('cit_gae_mailer_fallback_email', ''),
    '#description' => t('If we fail sending the message from the email specified at sending time, we will always fall-back to this email address, see GAE docs on !fall-back-docs', array('!fall-back-docs' => l(t('which emails you can send from'), 'https://developers.google.com/appengine/docs/php/mail/#Sending_Mail'))),
  );

  $form['#validate'][] = 'cit_gae_mailer_admin_settings_form_validate';

  return system_settings_form($form);
}

/**
 * Configuration settings form validation.
 */
function cit_gae_mailer_admin_settings_form_validate($form, &$form_state) {
}
